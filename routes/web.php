<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();


Route::get('/', 'mainController@index')->name('main');

/*products pages*/
Route::get('/product/{product}', 'ProductController@show')->name('productPage');
Route::get('/products/{type}', 'ProductController@index')->name('products');

/*basket*/
Route::get('basket', 'BasketController@index')->name('basket');
Route::post('/addtocart/', 'BasketController@save')->name('addtocart');
Route::post('/clearcart/', 'BasketController@clear')->name('clearcart');



Route::get('/home', 'HomeController@index')->name('home');


Route::group(['prefix' => 'admins', 'namespace' => 'Admin', 'middleware' => 'role:admin'], function(){

	/*main*/
	Route::get('/', 'AdminController@index')->name('adminka');

	/*Users*/
	Route::get('/users','UserController@index')->name('users');
	Route::get('/users/destroy/{user}','UserController@destroy')->name('users.destroy');

	/*Types*/
	Route::get('/types','TypeController@index')->name('types.index');
	Route::post('/types/store', 'TypeController@store')->name('types.store');
    Route::get('/types/edit/{type}', 'TypeController@edit')->name('types.edit');
	Route::post('/types/update/{type}', 'TypeController@update')->name('types.update');
    Route::get('/types/destroy/{type}', 'TypeController@destroy')->name('types.destroy');

    /*Providers*/
	Route::get('/providers','ProviderController@index')->name('providers.index');
	Route::post('/providers/store', 'ProviderController@store')->name('providers.store');
    Route::get('/providers/edit/{provider}', 'ProviderController@edit')->name('providers.edit');
	Route::post('/providers/update/{provider}', 'ProviderController@update')->name('providers.update');
    Route::get('/providers/destroy/{provider}', 'ProviderController@destroy')->name('providers.destroy');

    /*Proucts*/
	Route::get('/products','ProductController@index')->name('products.index');
	Route::post('/products/store', 'ProductController@store')->name('products.store');
    Route::get('/products/edit/{product}', 'ProductController@edit')->name('products.edit');
	Route::post('/products/update/{product}', 'ProductController@update')->name('products.update');
    Route::get('/products/destroy/{product}', 'ProductController@destroy')->name('products.destroy');

    /*Orders*/
    Route::get('/orders','OrderController@index')->name('orders.index');
    Route::get('/ordersstatustrue/{order}','OrderController@changestatustrue')->name('orders.changestatustrue');
    Route::get('/ordersstatusfalse/{order}','OrderController@changestatusfalse')->name('orders.changestatusfalse');

});
