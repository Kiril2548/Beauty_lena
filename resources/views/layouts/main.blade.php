<!DOCTYPE html>
<html>
<head>
<title>Shoping | Home</title>
<link href="{{ asset("css/bootstrap.css") }}" rel="stylesheet" type="text/css" media="all" />
<!-- Custom Theme files -->
<!--theme-style-->
<link href="{{ asset("css/style.css") }}" rel="stylesheet" type="text/css" media="all" />  
<!--//theme-style-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!--theme-style-->
<link href="{{ asset("css/style4.css") }}" rel="stylesheet" type="text/css" media="all" /> 
<!--//theme-style-->
<script src="{{ asset("js/jquery.min.js") }}"></script>
<!-- start-rate-->
<script src=" {{ asset("js/jstarbox.js") }} "></script>
    <link rel="stylesheet" href="{{ asset("css/jstarbox.css") }}" type="text/css" media="screen" charset="utf-8" />
        <script type="text/javascript">
            jQuery(function() {
            jQuery('.starbox').each(function() {
                var starbox = jQuery(this);
                    starbox.starbox({
                    average: starbox.attr('data-start-value'),
                    changeable: starbox.hasClass('unchangeable') ? false : starbox.hasClass('clickonce') ? 'once' : true,
                    ghosting: starbox.hasClass('ghosting'),
                    autoUpdateAverage: starbox.hasClass('autoupdate'),
                    buttons: starbox.hasClass('smooth') ? false : starbox.attr('data-button-count') || 5,
                    stars: starbox.attr('data-star-count') || 5
                    }).bind('starbox-value-changed', function(event, value) {
                    if(starbox.hasClass('random')) {
                    var val = Math.random();
                    starbox.next().text(' '+val);
                    return val;
                    } 
                })
            });
        });
        </script>
<!--//End-rate-->

</head>
<body>
<!--header-->
<div class="header">
<div class="container">
        <div class="head">
            <div class=" logo">
                <a href="{{ route('main') }}"><img src="{{ asset("images/logo.png") }}"  alt=""></a> 
            </div>
        </div>
    </div>
    <div class="header-top">
        <div class="container">
        <div class="col-sm-5 col-md-offset-2  header-login">
                    <ul >
                        <li><a href="{{ route('login') }}">Login</a></li>
                        <li><a href="{{ route('register') }}">Register</a></li>
                    </ul>
                </div>
                
            <div class="col-sm-5 header-social">        
                  
                    
            </div>
                <div class="clearfix"> </div>
        </div>
        </div>
        
        <div class="container">
        
            <div class="head-top">
            
         <div class="col-sm-8 col-md-offset-2 h_menu4">
                <nav class="navbar nav_bottom" role="navigation">
 
 <!-- Brand and toggle get grouped for better mobile display -->
  <div class="navbar-header nav_2">
      <button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
     
   </div> 
   <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
        <ul class="nav navbar-nav nav_1">
            <li><a class="color" href="{{ route('main') }}">Home</a></li>
            
            <li class="dropdown mega-dropdown active">
                <a class="color1" href="#" class="dropdown-toggle" data-toggle="dropdown">Сatalog<span class="caret"></span></a>              
                <div class="dropdown-menu ">
                    <div class="menu-top">
                        <div class="col1">
                            <div class="h_nav">
                                    <ul>
                                        @foreach($type as $t)
                                        <li><a href="{{ route('products',$t->id) }}">{{ $t->name }}</a></li>
                                        @endforeach
                                    </ul>   
                            </div>                          
                        </div>
                        
            </li>
           
       
        </ul>
     </div><!-- /.navbar-collapse -->

</nav>
            </div>
            <div class="col-sm-2 search-right">
                <ul class="heart">
                    <li><a class="play-icon popup-with-zoom-anim" href="#small-dialog"><i class="glyphicon glyphicon-search"> </i></a></li>
                    <li>
                        <a href="{{ route('basket') }}">
                            <img src="{{ asset("images/cart.png") }}" alt=""/>
                        </a>
                    </li>
                </ul>
                    <div class="clearfix"> </div>
                    
                        <!----->

                        <!---pop-up-box---->                      
            <link href="{{  asset ('css/popuo-box.css') }}" rel="stylesheet" type="text/css" media="all"/>
            <script src="{{  asset("js/jquery.magnific-popup.js")}}" type="text/javascript"></script>
            <!---//pop-up-box---->
            <div id="small-dialog" class="mfp-hide">
                <div class="search-top">
                    <div class="login-search">
                        <input type="submit" value="">
                        <input type="text" value="Search.." onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search..';}">       
                    </div>
                </div>              
            </div>
         <script>
            $(document).ready(function() {
            $('.popup-with-zoom-anim').magnificPopup({
            type: 'inline',
            fixedContentPos: false,
            fixedBgPos: true,
            overflowY: 'auto',
            closeBtnInside: true,
            preloader: false,
            midClick: true,
            removalDelay: 300,
            mainClass: 'my-mfp-zoom-in'
            });
                                                                                        
            });
        </script>       
                        <!----->
            </div>
            <div class="clearfix"></div>
        </div>  
    </div>  
</div>
<!--banner-->

@yield('content')

    <div class="footer">
    <div class="footer-middle">
                <div class="container">
                    <div class="col-md-5 footer-middle-in">
                        <p>Welcome to Shopin! We are  the best group of nail technicians in the country, our technicians have  been in the industry over thirty years running salons, training  schools and nail supplies.</p>
                    </div>
                    
                    <div class="col-md-5 footer-middle-in">
                        <ul class=" in">
                            <li><a href="{{ route('main') }}">Returns</a></li>
                        </ul>
                        <ul class="in in1">
                            <li><a href="{{ route('login') }}">Login</a></li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"> </div>
                </div>
            </div>
            <div class="footer-bottom">
                <div class="container">
                
                    <p class="footer-class">&copy; 2018. All Rights Reserved </p>
                    <div class="clearfix"> </div>
                </div>
            </div>
        </div>
        <!--//footer-->
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="{{ asset("js/simpleCart.min.js") }}"> </script>
<!-- slide -->
<script src="{{ asset("js/bootstrap.min.js") }}"></script>
<!--light-box-files -->
<script src="{{ asset("js/jquery.chocolat.js") }}"></script>
<link rel="stylesheet" href="{{ asset("css/chocolat.css") }}" type="text/css" media="screen" charset="utf-8">
<!--light-box-files -->
<script type="text/javascript" charset="utf-8">
$(function() {
    $('a.picture').Chocolat();
});
</script>


</body>
</html>
