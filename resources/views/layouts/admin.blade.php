<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Admin Panel</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons">
    <link rel="stylesheet" href="https://unpkg.com/bootstrap-material-design@4.1.1/dist/css/bootstrap-material-design.min.css" integrity="sha384-wXznGJNEXNG1NFsbm0ugrLFMQPWswR3lds2VeinahP8N0zJw9VWSopbjv2x7WCvX" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
</head>
<body>
	<style>
		body{
			background-color: #fff;
			overflow-x: hidden;
		}
	</style>
    <div class="conteiner-fluid">
    <div class="row">
        <div class="col-md-12 d-flex navbar navbar-dark bg-dark justify-content-around">
            <a class="btn btn-light" href="{{ route('main') }}">Home</a>
            <a class="btn btn-light" href="{{ route('adminka') }}">Main</a>
            <a class="btn btn-light" href="{{ route('users') }}">Users</a>
            <a class="btn btn-light" href="{{ route('orders.index') }}">Orders</a>
            <a class="btn btn-light" href="{{route('products.index')}}">Products</a>
            <a class="btn btn-light" href="{{ route('types.index') }}">Types</a>
            <a class="btn btn-light" href="{{ route('providers.index') }}">Providers</a>
        </div>
    </div>
    @yield('content')
    </div>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/popper.js@1.12.6/dist/umd/popper.js" integrity="sha384-fA23ZRQ3G/J53mElWqVJEGJzU0sTs+SvzG8fXVWP+kJQ1lwFAOkcUOysnlKJC33U" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/bootstrap-material-design@4.1.1/dist/js/bootstrap-material-design.js" integrity="sha384-CauSuKpEqAFajSpkdjv3z9t8E7RlpJ1UP0lKM/+NdtSarroVKu069AlsRPKkFBz9" crossorigin="anonymous"></script>
</body>
</html>




