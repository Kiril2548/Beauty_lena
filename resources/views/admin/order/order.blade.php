@extends('layouts.admin')
@section('content')

<div class="container">
	<div class="row">
		<table class="table">
			<thead>
				<th>Id</th>
				<th>Status</th>
				<th>Product</th>
				<th>Amount</th>
				<th>Total price</th>
				<th>User</th>
				<th>Date</th>
				<th>Menu</th>
			</thead>
			<tbody>
				@foreach($orders as $o)
				<tr>
					<td>{{ $o->id }}</td>
					<td>
						@if($o->status == 2)
							Wait
						@elseif($o->status == 1)
							Success
						@else
							Disable
						@endif
					</td>
					<td>{{ $o->products->title }}</td>
					<td>{{ $o->amount }}</td>
					<td>{{ $o->total_price }}</td>
					<td>{{ $o->users->name  }}</td>
					<td>{{ $o->created_at }}</td>
					<td>
						
						<a href="{{ route('orders.changestatustrue', $o->id) }}" title="Good"><i class="fas fa-check-square"></i></a>
						<a href="{{ route('orders.changestatusfalse', $o->id) }}" title="Bad"><i class="fas fa-ban"></i></a>
						
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>
@endsection