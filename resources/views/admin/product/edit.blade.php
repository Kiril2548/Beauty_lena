@extends('layouts.admin')

@section('content')
<div class="container">
	<div class="row">
		<div class="col md-12 p-3 m-3" style="background-color: #fff; border-radius: 10px;">
			<h3>Edit product</h3>
			<form action="{{route('products.update', $product->id )}}" method="post">
				{{ csrf_field() }}
				<input value="{{ $product->title }}" type="text" name="title" placeholder="Title" class="form-control">
				<select name="type_id" class="custom-select mr-sm-2" id="inlineFormCustomSelect">
					<option value="{{ $product->type->id }}" selected>{{ $product->type->name }}</option>
					@foreach($type as $tp)
					<option value="{{ $tp->id }}">{{ $tp->name }}</option>
					@endforeach
				</select>
				<input value="{{ $product->price }}" type="number" name="price" placeholder="Price" class="form-control">
				<select name="provider_id" class="custom-select mr-sm-2" id="inlineFormCustomSelect">
					<option value="{{ $product->providers->id }}" selected>{{$product->providers->title}}</option>
					@foreach($provider as $p)
					<option value="{{ $p->id }}">{{ $p->title }}</option>
					@endforeach
				</select>
				<textarea name="text" id="" cols="30" rows="10" class="form-control" placeholder="Description">{{ $product->text }}</textarea>
				<input value="{{ $product->text }}" type="textarea" name="text" placeholder="Description" class="form-control">
				<input type="submit" class="btn btn-block btn-sm btn-success" value="Save">
			</form>
		</div>
	</div>
</div>

	
	

{{-- route('users.destroy' , $user->id) --}}
@endsection
