@extends('layouts.admin')
@section('content')
<div class="container">
	<div class="row">
		<div class="col md-12 p-3 m-3" style="background-color: #fff; border-radius: 10px;">
			<h3>Add product</h3>
			<form action="{{route('products.store')}}" method="post">
				{{ csrf_field() }}
				<input type="text" name="title" placeholder="Title" class="form-control">
				<select name="type_id" class="custom-select mr-sm-2" id="inlineFormCustomSelect">
					<option selected>Choose category</option>
					@foreach($type as $tp)
					<option value="{{ $tp->id }}">{{ $tp->name }}</option>
					@endforeach
				</select>
				<input type="number" name="price" placeholder="Price" class="form-control">
				<select name="provider_id" class="custom-select mr-sm-2" id="inlineFormCustomSelect">
					<option selected>Choose provider</option>
					@foreach($provider as $p)
					<option value="{{ $p->id }}">{{ $p->title }}</option>
					@endforeach
				</select>
				<textarea name="text" id="" cols="30" rows="10" class="form-control" placeholder="Description"></textarea>
				<input type="submit" class="btn btn-block btn-sm btn-success" value="Save">
			</form>
		</div>
		<table class="table">
			<thead>
				<th>Title</th>
				<th>Type</th>
				<th>Price</th>
				<th>Provider</th>
				<th>Description</th>
				<th>Date</th>
				<th>Menu</th>
			</thead>
			<tbody>
				@foreach($product as $t)
				<tr>
					<td>{{ $t->title }}</td>
					<td>{{ $t->type->name }}</td>
					<td>{{ $t->price }}</td>
					<td>{{ $t->providers->title }}</td>
					<td>{{ $t->text }}</td>

					<td>{{ $t->created_at }}</td>
					<td>
						<a href="{{route('products.edit', $t->id)}}" title="Edit"><i class="fas fa-cog"></i></a>
						<a href="{{route('products.destroy', $t->id)}}" title="Delete"><i class="fas fa-trash"></i></a>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>


{{-- route('users.destroy' , $user->id) --}}
@endsection