@extends('layouts.admin')

@section('content')
<table class="table">
	<thead>
		<th>ID</th>
		<th>Name</th>
		<th>Email</th>
		<th>Phone</th>
		<th>Date</th>
		<th>Menu</th>
	</thead>
	<tbody>
		@foreach($users as $user)
			<tr>
				<td>{{ $user->id }}</td>
				<td>{{ $user->name }}</td>
				<td>{{ $user->email }}</td>
				<td>{{ $user->ph_number }}</td>
				<td>{{ $user->created_at }}</td>
				<td>
					<a href="{{ route('users.destroy' , $user->id) }}" <i class="fas fa-trash"></i></a>
				</td>
			</tr>
		@endforeach
	</tbody>
</table>
@endsection