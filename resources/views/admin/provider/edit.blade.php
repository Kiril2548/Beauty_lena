@extends('layouts.admin')

@section('content')
<div class="container">
	<div class="row">
		<div class="col md-12 p-3 m-3" style="background-color: #fff; border-radius: 10px;">
			<h3>Edit provider</h3>
			<form action="{{route('providers.update', $provider->id )}}" method="post">
				{{ csrf_field() }}
				<input type="text" name="title" placeholder="Name of company"  class="form-control" value="{{ $provider->title }}">
				<input type="text" name="email" placeholder="Email" class="form-control" value="{{ $provider->email }}">
				<input type="text" name="ph_number" placeholder="Phone number" class="form-control" value="{{ $provider->ph_number }}">
				<input type="text" name="country" placeholder="Country" class="form-control" value="{{ $provider->country }}">
				<textarea name="description" id="" cols="30" rows="10" class="form-control" placeholder="Description">
					{{ $provider->description }}
				</textarea>
				<input type="submit" class="btn btn-block btn-sm btn-success" value="Edit"> 
			</form> 
		</div>
	</div>
</div>

	
	

{{-- route('users.destroy' , $user->id) --}}
@endsection
