@extends('layouts.admin')

@section('content')
<div class="container">
	<div class="row">
		<div class="col md-12 p-3 m-3" style="background-color: #fff; border-radius: 10px;">
			<h3>Add new provider</h3>
			<form action="{{route('providers.store')}}" method="post">
				{{ csrf_field() }}
				<input type="text" name="title" placeholder="Name of company" class="form-control">
				<input type="text" name="email" placeholder="Email" class="form-control">
				<input type="text" name="ph_number" placeholder="Phone number" class="form-control">
				<input type="text" name="country" placeholder="Country" class="form-control">
				<textarea class="form-control" name="description" id="" cols="30" rows="10" placeholder="Description"></textarea>
				<input type="submit" class="btn btn-block btn-sm btn-success" value="Save"> 
			</form> 
		</div>
		<table class="table">
			<thead>
				<th>Title</th>
				<th>Email</th>
				<th>Phone</th>
				<th>Country</th>
				<th>Description</th>
				<th>Date</th>
				<th>Menu</th>
			</thead>
			<tbody>
				@foreach($provider as $t)
				<tr>
					<td>{{ $t->title }}</td>
					<td>{{ $t->email }}</td>
					<td>{{ $t->ph_number }}</td>
					<td>{{ $t->country }}</td>
					<td>{{ $t->description }}</td>
					<td>{{ $t->created_at }}</td>
					<td>
						<a href="{{route('providers.edit', $t->id)}}" title="Edit"><i class="fas fa-cog"></i></a>
						<a href="{{route('providers.destroy', $t->id)}}" title="Delete"><i class="fas fa-trash"></i></a>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>

	
	

{{-- route('users.destroy' , $user->id) --}}
@endsection