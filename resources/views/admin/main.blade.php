@extends('layouts.admin')

@section('content')


<div class="row">
	<div class="col-md-6 p-3">
		<div class="card" style="width: 100%;">
		  <div class="card-body">
		    <h5 class="card-title">Users</h5>
		    <h6 class="card-subtitle mb-2 text-muted">Number of users: <strong>{{ $user }}</strong></h6>

		    <a href="{{ route('users') }}" class="card-link">All users</a>
		    <a href="#" class="card-link"></a>
		  </div>
		</div>
	</div>
	<div class="col-md-6 p-3">
		<div class="card" style="width: 100%;">
		  <div class="card-body">
		    <h5 class="card-title">Orders</h5>
		    <h6 class="card-subtitle mb-2 text-muted">Number of orders: <strong>{{ $order }}</strong></h6>

		    <a href="{{ route('orders.index') }}" class="card-link">All orders</a>
		    <!-- <a href="#" class="card-link">Посмотреть не обработаные заказы</a> -->
		  </div>
		</div>
	</div>
	<div class="col-md-4 p-3">
		<div class="card" style="width: 100%;">
		  <div class="card-body">
		    <h5 class="card-title">Products</h5>
		    <h6 class="card-subtitle mb-2 text-muted">Number of products : <strong>{{ $product }}</strong></h6>

		    <a href="{{ route('products.index') }}" class="card-link">All products</a>
		  </div>
		</div>
	</div>
	<div class="col-md-4 p-3">
		<div class="card" style="width: 100%;">
		  <div class="card-body">
		    <h5 class="card-title">Types</h5>
		    <h6 class="card-subtitle mb-2 text-muted">Number of types: <strong>{{ $type }}</strong></h6>

		    <a href="{{ route('types.index') }}" class="card-link">All types</a>
		  </div>
		</div>
	</div>
	<div class="col-md-4 p-3">
		<div class="card" style="width: 100%;">
		  <div class="card-body">
		    <h5 class="card-title">Providers</h5>
		    <h6 class="card-subtitle mb-2 text-muted">Number of providers: <strong>{{ $provider }}</strong></h6>
		    <a href="{{ route('providers.index') }}" class="card-link">All providers</a>
		  </div>
		</div>
	</div>
</div>

@endsection