@extends('layouts.admin')

@section('content')
<div class="container">
	<div class="row">
		<div class="col md-12 p-3 m-3" style="background-color: #fff; border-radius: 10px;">
			<h3>Add new type</h3>
			<form action="{{route('types.store')}}" method="post">
				{{ csrf_field() }}
				<input type="text" name="name" placeholder="Type name" class="form-control">
				<input type="submit" class="btn btn-block btn-sm btn-success" value="Save"> 
			</form> 
		</div>
		<table class="table">
			<thead>
				<th>Name</th>
				<th>Menu</th>
			</thead>
			<tbody>
				@foreach($type as $t)
				<tr>
					<td>{{ $t->name }}</td>
					<td>
						<a href="{{route('types.edit', $t->id)}}" title="Edit"><i class="fas fa-cog"></i></a>
						<a href="{{route('types.destroy', $t->id)}}" title="Delete"><i class="fas fa-trash"></i></a>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>

	
	

{{-- route('users.destroy' , $user->id) --}}
@endsection