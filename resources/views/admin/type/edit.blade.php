@extends('layouts.admin')

@section('content')
<div class="container">
	<div class="row">
		<div class="col md-12 p-3 m-3" style="background-color: #fff; border-radius: 10px;">
			<h3>Изменить тип</h3>
			<form action="{{route('types.update', $type->id )}}" method="post">
				{{ csrf_field() }}
				<input type="text" name="name" placeholder="Type name" class="form-control" value="{{ $type->name }}">
				<input type="submit" class="btn btn-block btn-sm btn-success" value="Edit"> 
			</form> 
		</div>
	</div>
</div>

	
	

{{-- route('users.destroy' , $user->id) --}}
@endsection
