@extends('layouts.main')
@section('content')
@inject('session', 'Session')
@if(!empty($session::get('msg')))
	<h1>{{ $session::get('msg') }}</h1>
	<?php
		$session::flush();
	?>
@endif 

@if($mes == 0)
<div class="conainer" style="margin-left: 3%; margin-right: 3%;">
	<div class="row">
		<table class="table">
			<thead>
				<tr>
					<th>IMAGE</th>
					<th>Product</th>
					<th>Amount</th>
					<th>Price</th>
				</tr>
			</thead>
			<tbody>
				@foreach ( $products as $product )
				<tr>
					<td><img src="" alt=""></td>
					<td>{{ $product['product']['title'] }}</td>
					<td>{{ $product['amount'] }}</td>
					<td><b>{{ $product['amount']*$product['product']['price'] }} USD</b></td>
				</tr>
				@endforeach
			</tbody>
		</table>
		<form action="{{ route('clearcart') }}" method="post">
			{{ csrf_field() }}
			<input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
			<button class="btn btn-lg btn-danger">Buy</button>
		</form>

	</div>
</div>
@else
<h1>Your cart is empty!</h1>
@endif

@endsection