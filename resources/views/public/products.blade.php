@extends('layouts.main')
@section('content')
@foreach( $products as $product )
@endforeach
<!--content-->
<div class="product">
	<div class="container">
		<div class="row">
			<div class="col-md-9">
				<div class="mid-popular">
					@foreach ( $products as $product )
					<div class="col-md-4 item-grid1 simpleCart_shelfItem">
						<div class=" mid-pop">
							<div class="pro-img">
								<div class="zoom-icon ">
									<a class="picture" href="images/pc.jpg" rel="title" class="b-link-stripe b-animate-go  thickbox"><i class="glyphicon glyphicon-search icon "></i></a>
									<a href="{{ route('productPage', $product->id) }}"><i class="glyphicon glyphicon-menu-right icon"></i></a>
								</div>
							</div>
							<div class="mid-1">
								<div class="women">
									<div class="women-top">
										<span>{{ $product->title }}</span>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="mid-1">
									<p ><label>{{ $product->price}} USD</label></p>
									
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
					</div>
					@endforeach
				</div>
			</div>
			
			<div class="col-md-3 product-bottom product-at">
				<!--categories-->
				<div class=" rsidebar span_1_of_left">
					<h4 class="cate">Categories</h4>
					<ul class="menu-drop">
						@foreach($type as $t)
						<li><a href="{{ route('products', $t->id) }}">{{ $t->name }} </a></li>
						@endforeach
					</ul>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	
</div>
<!--//menu-->

</div>
</section>
</div>
<div class="clearfix"></div>
</div>
@endsection