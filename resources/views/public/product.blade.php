@extends('layouts.main')
@section('content')


<div class="single">
	<div class="container">
		<div class="col-md-9">
			
			<div class="col-md-7 single-top-in">
				<div class="span_2_of_a1 simpleCart_shelfItem">
					<h3>{{ $product[0]['title']  }}</h3>
					<div class="price_single">
						<span class="reducedfrom item_price">{{ $product[0]['price']  }} USD </span>
						<div class="clearfix"></div>
					</div>
					<h4 class="quick">Quick Overview:</h4>
					<p class="quick_desc"> {{ $product[0]['text'] }}</p>
					<form action=" {{ route('addtocart') }} " method="post">
						{{ csrf_field() }}
						<input type="hidden" name="product_id" value="{{ $product[0]['id'] }}">
						<div class="quantity">
							<div class="quantity-select">
								<div class="entry value-minus">&nbsp;</div>
								<input type="text" name="amount" class="entry value" value="1">
								<div class="entry value-plus active">&nbsp;</div>
							</div>
						</div>
						<!--quantity-->
						<script>
							$('.value-plus').click(function(){
								var v = +($(".value").val());

								$(".value").val(v+1);
							});
							$('.value-minus').click(function(){
								var v = +($(".value").val());
								if(v>1)
									$(".value").val(v-1);
							});
						</script>
						<!--quantity-->
						
						<button class="add-to item_add hvr-skew-backward">Add to cart</button>
					</form>
					<div class="clearfix"> </div>
				</div>
				
			</div>
			<div class="clearfix"> </div>
			<!---->
			<div class="tab-head">
				<nav class="nav-sidebar">
					<ul class="nav tabs">
						<li class=""><a href="#tab2" data-toggle="tab">Provider information</a></li>
					</ul>
				</nav>
				<div class="tab-content one">
					<div class="tab-pane active text-style" id="tab1">
						<div class="facts">
							<p >{{ $product[0]['providers']['description'] }} </p>
						</div>
					</div>
					
				</div>
				<div class="clearfix"></div>
			</div>
			<!---->
		</div>
		<!----->
		<div class="col-md-3 product-bottom product-at">
			<!--categories-->
			<div class=" rsidebar span_1_of_left">
				<h4 class="cate">Categories</h4>
				<ul class="menu-drop">
					@foreach($type as $t)
					<li><a href="{{ route('products',$t->id) }}">{{ $t->name }} </a></li>
					@endforeach
				</ul>
		</div>
		<!--//menu-->
		
		<div class="clearfix"> </div>
	</div>
	
	<!--brand-->
	<div class="container">
		<div class="brand">
			
			<div class="clearfix"></div>
		</div>
	</div>
	<!--//brand-->
</div>
<!--//content-->
@endsection