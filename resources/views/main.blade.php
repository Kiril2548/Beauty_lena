@extends('layouts.main')

@section('content')

<div class="banner">
<div class="container">
<section class="rw-wrapper">
				<h1 class="rw-sentence">
					<span>Everything for nails</span>
					<div class="rw-words rw-words-1">
						<span>Wide assortment</span>
						<span>High quality</span>
						<span>Fast delivery</span>
						<span>The best suppliers</span>
						<span>Constant discounts</span>
						<span>Loyalty systems</span>
					</div>
					<div class="rw-words rw-words-2">
						<span>Reasnable prices</span>
						<span>Сlassic looks</span>
						<span>Latest trends</span>
						<span>Everything you need</span>
					</div>
				</h1>
			</section>
			</div>
</div>
	<!--content-->
		<div class="content">
			<div class="container">
				<div class="content-top">
					<div class="col-md-6 col-md">
						<div class="col-1">
						 <a  class="b-link-stroke b-animate-go  thickbox">
		   <img src="images/pi11.jpg" class="img-responsive" alt=""/><div class="b-wrapper1 long-img"><p class="b-animate b-from-right    b-delay03 ">Kits</p><label class="b-animate b-from-right    b-delay03 "></label><h3 class="b-animate b-from-left    b-delay03 ">Cheaper together</h3></div></a>

							<!---<a href="single.html"><img src="images/pi.jpg" class="img-responsive" alt=""></a>-->
						</div>
						<div class="col-2">
						</div>
					</div>
					<div class="col-md-6 col-md1">
						<div class="col-3">
							<a ><img src="images/pi33.jpg" class="img-responsive" alt="">
							<div class="col-pic">
								<p>Brushes</p>
								<label></label>
								<h5>For a special design</h5>
							</div></a>
						</div>
						<div class="col-3">
							<a><img src="images/pi22.jpg" class="img-responsive" alt="">
							<div class="col-pic">
								<p>Milling machines</p>
								<label></label>
								<h5>For perfect manicure</h5> 
						</div>
						<div class="col-3">
							
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
				<!--products-->
			<div class="content-mid">
				<h3>Trending Items</h3>
				<label class="line"></label>
				<div class="mid-popular">
					@foreach($product as $prod)
					<div class="col-md-3 item-grid simpleCart_shelfItem">	
					<div class=" mid-pop">
					<div class="pro-img">
						<div class="zoom-icon ">
						<a href="{{ route('productPage', $prod->id) }}"><i class="glyphicon glyphicon-menu-right icon"></i></a>
						</div>
						</div>
						<div class="mid-1">
						<div class="women">
						<div class="women-top">
							<span>{{ $prod->type->name }}</span>
							<h6><a href="single.html">{{ $prod->title }}</a></h6>
							</div>
							<div class="clearfix"></div>
							</div>
							<div class="mid-2">
								<p ><label>{{ $prod->price*1.2 }}USD</label><em class="item_price">{{ $prod->price }}USD</em></p>
								<div class="clearfix"></div>
							</div>
							
						</div>
					</div>
					</div>
					@endforeach
				</div>
			</div>
			<!--//products-->
			</div>
		</div>
	<!--//content-->
	<br>

@endsection