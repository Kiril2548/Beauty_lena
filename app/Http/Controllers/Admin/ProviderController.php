<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Provider;

class ProviderController extends Controller
{
    public function index()
    {
    	$provider = Provider::all();
        return view('admin.provider.providers',compact('provider'));
    }

    public function store(Request $request)
    {
        
        $this->validate($request,[
            'title'              => 'required|max:50',
            'email'              => 'required|max:50',
            'ph_number'          => 'required|max:13',
            'country'            => 'required|max:50',
            'description'        => 'required|max:3000',
        ]);

        $res = $request->All();
        $provider = new Provider();

        $provider->fill($request->only(['title','email','ph_number','country','description']));
        $provider->save();

        return redirect()->route('providers.index');
    }

    public function edit(Provider $provider)
    {
        return view('admin.provider.edit',compact('provider'));
    }

    public function update(Request $request, Provider $provider)
    {   
        $this->validate($request,[
            'title'              => 'required|max:255',
            'email'              => 'required|max:255',
            'ph_number'              => 'required|max:13',
            'country'              => 'required|max:255',
            'description'              => 'required|max:3000',
        ]);

        $provider->fill($request->only(['title','email','ph_number','country','description']));
        $provider->update();

        return redirect()->route('providers.index');
    }

    public function destroy(Provider $provider)
    {
        $provider->delete();
        return redirect()->route('providers.index');
    }
}
