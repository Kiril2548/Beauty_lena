<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Type;

class TypeController extends Controller
{
    public function index()
    {
    	$type = Type::all();
        return view('admin.type.types',compact('type'));
    }

    public function store(Request $request)
    {
        
        $this->validate($request,[
            'name'              => 'required|max:50',
        ]);

        $res = $request->All();
        $type = new Type();

        $type->fill($request->only(['name']));
        $type->save();

        return redirect()->route('types.index');
    }

    public function edit(Type $type)
    {
        return view('admin.type.edit',compact('type'));
    }

    public function update(Request $request, Type $type)
    {   
        $this->validate($request,[
            'name'              => 'required|max:50',
        ]);

        $type->fill($request->only(['name']));
        $type->update();

        return redirect()->route('types.index');
    }

    public function destroy(Type $type)
    {
        $type->delete();
        return redirect()->route('types.index');
    }
}
