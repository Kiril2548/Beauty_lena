<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\User;

class UserController extends Controller
{
    public function index(){
    	$users = User::all();
    	return view('admin.user.users',compact('users'));
    }


    public function destroy(User $user)
    {
        $user->delete();
        return redirect()->route('users');
    }
}
