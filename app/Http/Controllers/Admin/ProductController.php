<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB;
use App\Product;
use App\Type;
use App\Provider;

class ProductController extends Controller
{
    public function index()
    {
        $type = Type::all();
        $provider = Provider::all();
    	$product = Product::with('type','providers')->get();
        return view('admin.product.products',compact('product', 'type', 'provider'));
    }

    public function store(Request $request)
    {        
        $this->validate($request,[
            'title'          => 'required|max:50',
            'type_id'        => 'required',
            'price'          => 'required',
            'provider_id'    => 'required',
            'text'           => 'required',
        ]);

        $res = $request->All();
        $product = new Product();

        $product->fill($request->only(['title','type_id','price','provider_id', 'text']));
        $product->save();

        return redirect()->route('products.index');
    }

    public function edit(Product $product)
    {
        $type = Type::all();
        $provider = Provider::all();
        return view('admin.product.edit',compact('product', 'type', 'provider'));
    }

    public function update(Request $request, Product $product)
    {   
        $this->validate($request,[
            'title'              => 'required|max:50',
            'type_id'              => 'required',
            'price'          => 'required',
            'provider_id'            => 'required',
            'text'           => 'required',
        ]);

        $product->fill($request->only(['title','type_id','price','provider_id', text]));
        $product->update();

        return redirect()->route('products.index');
    }

    public function destroy(Product $product)
    {
        $product->delete();
        return redirect()->route('products.index');
    }
}
