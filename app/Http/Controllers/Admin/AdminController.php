<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\User;
use App\Type;
use App\Provider;
use App\Product;
use App\Order;
use Session;
class AdminController extends Controller
{
   public function index(){
   	$user = User::count();
   	$type = Type::count();
   	$provider = Provider::count();
   	$product = Product::count();
      $order = Order::count();

   	// Session::put('item_id', ['20', '30', '25']);
   	// Session::put('key', 'true');
   	// $session = Session::all();
   	// dd($session);

   	 return view('admin.main', compact('user', 'type', 'provider', 'product', 'order'));
   }
}
