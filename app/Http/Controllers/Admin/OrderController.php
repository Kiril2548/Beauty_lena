<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB;
use App\Order;



class OrderController extends Controller
{
    
    public function index()
    {
    	$orders = Order::with('users','products')->get();
    	//dd($orders->toArray());

        return view('admin.order.order',compact('orders'));
    }

    public function changestatustrue(Order $order){

        $order->fill([
        	'status' => 1,
        ]);

        $order->update();

   		return redirect()->back();
    }
    public function changestatusfalse(Order $order){

        $order->fill([
        	'status' => 0,
        ]);

        $order->update();

   		return redirect()->back();
    }

}
