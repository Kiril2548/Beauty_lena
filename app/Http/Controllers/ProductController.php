<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Type;
use DB;

class ProductController extends Controller
{
	public function index($id){
		$products = Product::where('type_id', $id)->with('type', 'providers')->get();
		$type = Type::all();
		return view('public.products', compact('type','products'));
	}

    public function show(Product $product){
    	$product = Product::where('id', $product->id)->with('type', 'providers')->get();
    	$type = Type::all();
    	return view('public.product', compact('product', 'type'));
    }
}
