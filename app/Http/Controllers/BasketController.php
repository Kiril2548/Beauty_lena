<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\Type;
use App\Product;
use App\Order;

class BasketController extends Controller
{
	public function index (){
		$session = Session::all();
		$type = Type::all();
		$products = array();
		if (!empty($session['basket'])){
			foreach ($session['basket'] as $value) {
				$t = Product::where('id',$value['product_id'])->with('type', 'providers')->get();
				$t = $t[0]->toArray();
				$p['product'] = $t;
				$p['amount'] = $value['amount'];
				array_push($products, $p);
			}
			$mes = 0;
			return view('public.basket', compact('products','type','mes'));
		}
		else{
			$mes = 1;
			return view('public.basket', compact('mes','type'));
		}
	}
    
    public function save(Request $request){
    	//if(!empty(Auth::user()->id)){
    		Session::push('basket', [
    	 		'product_id' => $request->product_id,
    	 		'amount'     => $request->amount ,
	    	 ]);
	    	return redirect()->back();
    	//}
    	//else{
    	//	return redirect()->route('main');
    	//}
    	
    }

    public function clear(Request $request){
    	$session = Session::all();
		foreach ($session['basket'] as $value) {
			$t = Product::where('id',$value['product_id'])->with('type', 'providers')->get();
			$t = $t[0]->toArray();
			$order = new Order();
        	$order->fill([
        		'user_id'		=> $request->user_id,
        		'product_id'	=> $value['product_id'],
        		'status' 		=> 2,
        		'amount' 		=> $value['amount'],
        		'total_price' 	=> $t['price']*$value['amount'],
        	]);
        	$order->save();
		}
		Session::forget('basket');
		return redirect()->back()->with(['mes' => 'Заказ успешно оформлен ожимайте мы с вами свяжемся']);
 	}

}
