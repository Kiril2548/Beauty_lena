<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Type;
use App\Product;
use App\Provider;

class mainController extends Controller
{
     public function index()
    {
    	$product = Product::with('type')->get();
    	$type = Type::all();
        return view('main', compact( 'product','type'));  
    }
  
}
