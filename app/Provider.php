<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Provider extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title','email','ph_number','country','description',
    ];

    public function providers()
    {
        return $this->hasMany('App\Product');
    }
}
