<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'title','type_id','price','provider_id', 'text',
    ];

    public function type()
    {
        return $this->belongsTo('App\Type');
    }

    public function providers()
    {
        return $this->belongsTo('App\Provider', 'provider_id', 'id');
    }


     public function products()
    {
        return $this->hasMany('App\Order');
    }

}
